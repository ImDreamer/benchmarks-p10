﻿using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro._2048; 

public class Optimal {
	public static ulong Iterations;
	public static ulong LoopIterations;

	internal struct Tile {
		public Tile() {
			Value = 0;
			IsBlocked = false;
		}

		public uint Value { get; set; }
		public bool IsBlocked { get; set; }
	}

	internal enum MoveDirection {
		Up,
		Down,
		Left,
		Right
	}

	internal struct G2048 {
		public G2048() {
			_isDone = default;
			_isWon = default;
			_isMoved = default;
			_score = default;
			_inputs = null;
		}
		
		public void InitializeBoard() {
			_isDone = false;
			_isWon = false;
			_isMoved = true;
			_score = 0;
			_inputs = new Queue<char>(Enumerable.Repeat('w', 100));
			for (uint y = 0; y < 4; y++) {
				for (uint x = 0; x < 4; x++) {
					_board[x, y] = new Tile();
				}
			}
		}

		private bool _isDone;
		private bool _isWon;
		private bool _isMoved;
		private uint _score;
		private Queue<char> _inputs;
		private readonly Tile[,] _board = new Tile[4, 4];
		private readonly Random _rand = new(2048);

		public void Loop() {
			AddTile();
			while (true) {
				if (_isMoved) {
					AddTile();
				}

				DrawBoard();
				if (_isDone) {
					break;
				}

				WaitKey();
			}

			string endMessage = _isWon ? "You've made it!" : "Game Over!";
			Console.WriteLine(endMessage);
		}

		private void DrawBoard() {
			Console.Clear();
			StringBuilder sb = new StringBuilder("Score: ");
			sb.Append(_score);
			sb.Append("\n");
			Console.WriteLine(sb.ToString());
			for (uint y = 0; y < 4; y++) {
				Console.WriteLine("+------+------+------+------+");
				Console.Write("| ");
				for (uint x = 0; x < 4; x++) {
					if (_board[x, y].Value == 0) {
						const string empty = " ";
						Console.Write(empty.PadRight(4));
					}
					else {
						Console.Write(_board[x, y].Value.ToString().PadRight(4));
					}

					Console.Write(" | ");
				}

				Console.WriteLine();
			}
			Console.WriteLine("+------+------+------+------+\n\n");
		}


		private void WaitKey() {
			_isMoved = false;

			if (_inputs.Count == 0) {
				_isDone = true;
				return;
			}

			switch (_inputs.Dequeue()) {
				case 'W':
					Move(MoveDirection.Up);
					break;
				case 'A':
					Move(MoveDirection.Left);
					break;
				case 'S':
					Move(MoveDirection.Down);
					break;
				case 'D':
					Move(MoveDirection.Right);
					break;
			}

			for (uint y = 0; y < 4; y++) {
				for (uint x = 0; x < 4; x++) {
					_board[x, y].IsBlocked = false;
				}
			}
		}

		private void AddTile() {
			for (uint y = 0; y < 4; y++) {
				for (uint x = 0; x < 4; x++) {
					if (_board[x, y].Value != 0) continue;
					uint a, b;
					do {
						a = (uint)_rand.Next(0, 4);
						b = (uint)_rand.Next(0, 4);
					} while (_board[a, b].Value != 0);

					double r = _rand.NextDouble();
					_board[a, b].Value = r > 0.89f ? 4u : 2u;

					if (CanMove()) {
						return;
					}
				}
			}

			_isDone = true;
		}

		private bool CanMove() {
			for (uint y = 0; y < 4; y++) {
				for (uint x = 0; x < 4; x++) {
					if (_board[x, y].Value == 0) {
						return true;
					}
				}
			}

			for (uint y = 0; y < 4; y++) {
				for (uint x = 0; x < 4; x++) {
					if (TestAdd(x + 1, y, _board[x, y].Value)
					    || TestAdd(x - 1, y, _board[x, y].Value)
					    || TestAdd(x, y + 1, _board[x, y].Value)
					    || TestAdd(x, y - 1, _board[x, y].Value)) {
						return true;
					}
				}
			}

			return false;
		}

		private bool TestAdd(uint x, uint y, uint value) {
			if (x < 0 || x > 3 || y < 0 || y > 3) {
				return false;
			}

			return _board[x, y].Value == value;
		}

		private void MoveVertically(uint x, uint y, int d) {
			if (_board[x, y + d].Value != 0
			    && _board[x, y + d].Value == _board[x, y].Value
			    && !_board[x, y].IsBlocked
			    && !_board[x, y + d].IsBlocked) {
				_board[x, y].Value = 0;
				_board[x, y + d].Value *= 2;
				_score += _board[x, y + d].Value;
				_board[x, y + d].IsBlocked = true;
				_isMoved = true;
			}
			else if (_board[x, y + d].Value == 0
			         && _board[x, y].Value != 0) {
				_board[x, y + d].Value = _board[x, y].Value;
				_board[x, y].Value = 0;
				_isMoved = true;
			}

			if (d > 0) {
				if (y + d < 3) {
					MoveVertically(x, (uint)(y + d), 1);
				}
			}
			else {
				if (y + d > 0) {
					MoveVertically(x, (uint)(y + d), -1);
				}
			}
		}

		private void MoveHorizontally(uint x, uint y, int d) {
			if (_board[x + d, y].Value != 0
			    && _board[x + d, y].Value == _board[x, y].Value
			    && !_board[x + d, y].IsBlocked
			    && !_board[x, y].IsBlocked) {
				_board[x, y].Value = 0;
				_board[x + d, y].Value *= 2;
				_score += _board[x + d, y].Value;
				_board[x + d, y].IsBlocked = true;
				_isMoved = true;
			}
			else if (_board[x + d, y].Value == 0
			         && _board[x, y].Value != 0) {
				_board[x + d, y].Value = _board[x, y].Value;
				_board[x, y].Value = 0;
				_isMoved = true;
			}

			if (d > 0) {
				if (x + d < 3) {
					MoveHorizontally((uint)(x + d), y, 1);
				}
			}
			else {
				if (x + d > 0) {
					MoveHorizontally((uint)(x + d), y, -1);
				}
			}
		}

		private void Move(MoveDirection direction) {
			switch (direction) {
				case MoveDirection.Up:
					for (uint x = 0; x < 4; x++) {
						uint y = 1;
						while (y < 4) {
							if (_board[x, y].Value != 0) {
								MoveVertically(x, y, -1);
							}

							y++;
						}
					}

					break;
				case MoveDirection.Down:
					for (uint x = 0; x < 4; x++) {
						uint y = 2;
						while (y >= 0) {
							if (_board[x, y].Value != 0) {
								MoveVertically(x, y, 1);
							}

							y--;
						}
					}

					break;
				case MoveDirection.Left:
					for (uint y = 0; y < 4; y++) {
						uint x = 1;
						while (x < 4) {
							if (_board[x, y].Value != 0) {
								MoveHorizontally(x, y, -1);
							}

							x++;
						}
					}

					break;
				case MoveDirection.Right:
					for (uint y = 0; y < 4; y++) {
						uint x = 2;
						while (x >= 0) {
							if (_board[x, y].Value != 0) {
								MoveHorizontally(x, y, 1);
							}

							x--;
						}
					}

					break;
			}
		}
	}

	[Benchmark("2048", "Test")]
	public static int Optimal2048() {
		var game = new G2048();
		for (ulong i = 0; i < LoopIterations; i++) {
			game.InitializeBoard();
			game.Loop();
		}


		return 13;
	}
}