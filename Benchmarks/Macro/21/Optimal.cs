﻿using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro._21;

public class Optimal {
	public static ulong Iterations;
	public static ulong LoopIterations;
	private const string computerPlayer = "Computer";
	private const string humanPlayer = "Player 1";

	public static string SwapPlayer(string currentPlayer) {
		switch (currentPlayer) {
			case computerPlayer:
				currentPlayer = humanPlayer;
				break;
			default:
				currentPlayer = computerPlayer;
				break;
		}

		return currentPlayer;
	}

	public static void PlayGame() {
		bool playAnother = true;
		uint total = 0;
		uint final = 21;
		uint roundChoice = 0;
		string currentPlayer = RandomPLayerSelect();
		uint compWins = 0;
		uint humanWins = 0;

		while (playAnother) {
			Console.WriteLine($"Now playing: {currentPlayer}");
			switch (currentPlayer) {
				case computerPlayer:
					roundChoice = CompMove(total);
					break;
				default:
					roundChoice = uint.Parse(Random.Next(1, 4).ToString());
					break;
			}


			if (roundChoice != 1 && roundChoice != 2 && roundChoice != 3) {
				Console.WriteLine("Invalid choice! Choose from numbers: 1, 2, 3.");
				continue;
			}

			total += roundChoice;

			Console.WriteLine(total);

			if (total == final) {
				switch (currentPlayer) {
					case computerPlayer:
						compWins++;
						break;
					case humanPlayer:
						humanWins++;
						break;
				}

				Console.WriteLine($"Winner: {currentPlayer}");
				StringBuilder sb3 = new StringBuilder("Comp wins: ");
				sb3.Append(compWins);
				sb3.Append(". Human wins: ");
				sb3.Append(humanWins);
				Console.WriteLine(sb3.ToString());
				Console.WriteLine("do you wan to play another round? y/n");
				string choice = "n";
				switch (choice) {
					case "y":
						total = 0;
						break;
					case "n":
						goto end;
						break;
					default:
						Console.WriteLine("Invalid choice! Choose from y or n");
						continue;
				}
			}

			else if (total > 21) {
				Console.WriteLine("Not the right time to play this game :)");
				break;
			}

			currentPlayer = SwapPlayer(currentPlayer);
		}
		
		end: ;
	}

	public static bool CheckIfCanWin(uint total) {
		bool result = false;
		if (total == 18) {
			result = true;
		}

		return result;
	}

	public static uint CompMove(uint total) {
		uint choice = 0;

		if (CheckIfCanWin(total)) {
			choice = 21u - total;
		}
		else {
			choice = (uint)new Random().Next(1, 4);
		}

		return choice;
	}

	public static string RandomPLayerSelect() {
		string[] players = new string[] { computerPlayer, humanPlayer };
		int random = new Random().Next(0, 2);
		return players[random];
	}

	public static Random Random = new Random(2048);

	[Benchmark("21", "Tests 21 using the default implementation")]
	public static uint Optimal21() {
		uint result = 2048u;
		for (ulong i = 0; i < LoopIterations; i++) {
			PlayGame();
		}

		return result;
	}
}