﻿using System.Globalization;
using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro._99BottlesOfBeer;

public class All {
	public static ulong Iterations;
	public static ulong LoopIterations;

	private const string Vessel = "bottle";
	private const string Beverage = "beer";
	private const string Location = "on the wall";

	private static string DefaultAction(ref uint bottles) {
		bottles--;
		return "take one down, pass it around,";
	}

	private static string FallbackAction(ref uint bottles) {
		bottles += 99;
		return "go to the store, buy some more,";
	}

	private static string Act(ref uint bottles) {
		return bottles > 0 ? DefaultAction(ref bottles) : FallbackAction(ref bottles);
	}

	[Benchmark("99BottlesOfBeer", "")]
	public static int All99BottlesOfBeer() {
		for (ulong i = 0; i < LoopIterations; i++) {
			Func<uint, string> plural = b => b == 1 ? "" : "s";
			Func<uint, string> describeCount = b => b == 0 ? "no more" : b.ToString();
			Func<string, string, string, string, string> describeBottles =
				(first, second, third, fourth) => new StringBuilder().Append(first)
					.Append(" ")
					.Append(second)
					.Append(third)
					.Append(" of ")
					.Append(fourth).ToString();
			Action<string> write = s => Console.WriteLine(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(s));
			uint bottles = 99;
			for (int y = 0; y < 199; y++) {
				StringBuilder sb = new StringBuilder();
				StringBuilder sb2 = new StringBuilder();
				sb.Append(describeBottles(describeCount(bottles), Vessel, plural(bottles), Beverage))
					.Append(" ")
					.Append(Location)
					.Append(", ")
					.Append(describeBottles(describeCount(bottles), Vessel, plural(bottles), Beverage))
					.Append(",");
				write(sb.ToString());
				write(Act(ref bottles));
				sb2.Append(describeBottles(describeCount(bottles), Vessel, plural(bottles), Beverage))
					.Append(" ")
					.Append(Location)
					.Append(".");
				write(sb2.ToString());
				write(string.Empty);
			}
		}

		return 2048;
	}
}