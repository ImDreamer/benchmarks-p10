﻿using CsharpRAPL.Benchmarking.Attributes;
using EdgeList = System.Collections.Generic.List<(uint node, double weight)>;

namespace Benchmarks.Macro.Dijkstra;

public class DataType {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("Dijkstra", "")]
	public static int DataTypeDijkstra() {
		for (ulong loopIteration = 0; loopIteration < LoopIterations; loopIteration++) {
			Graph graph = new Graph(6);
			Func<char, uint> id = c => (uint)(c - 'a');
			Func<uint, char> name = i => (char)(i + 'a');
			foreach (var (start, end, cost) in new[] {
					('a', 'b', 7u),
					('a', 'c', 9u),
					('a', 'f', 14u),
					('b', 'c', 10u),
					('b', 'd', 15u),
					('c', 'd', 11u),
					('c', 'f', 2u),
					('d', 'e', 6u),
					('e', 'f', 9u),
				}) {
				graph.AddEdge(id(start), id(end), cost);
			}

			(double distance, uint prev)[] path = graph.FindPath(id('a'));
			for (uint d = id('b'); d <= id('f'); d++) {
				Console.WriteLine(string.Join(" -> ",
					Path(id('a'), d).Select(p => $"{name(p.node)}({p.distance})").Reverse()));
			}

			IEnumerable<(double distance, uint node)> Path(uint start, uint destination) {
				yield return (path[destination].distance, destination);
				for (uint i = destination; i != start; i = path[i].prev) {
					yield return (path[path[i].prev].distance, path[i].prev);
				}
			}
		}
		return 2048;
	}


	internal sealed class Graph {
		private readonly List<EdgeList> adjacency;

		public Graph(uint vertexCount) =>
			adjacency = Enumerable.Range(0, (int)vertexCount).Select(v => new EdgeList()).ToList();

		public uint Count => (uint)adjacency.Count;
		public bool HasEdge(uint s, uint e) => adjacency[(int)s].Any(p => p.node == e);
		public bool RemoveEdge(uint s, uint e) => adjacency[(int)s].RemoveAll(p => p.node == e) > 0;

		public bool AddEdge(uint s, uint e, double weight) {
			if (HasEdge(s, e)) return false;
			adjacency[(int)s].Add((e, weight));
			return true;
		}

		public (double distance, uint prev)[] FindPath(uint start) {
			(double distance, uint prev)[] info = Enumerable.Range(0, adjacency.Count).Select(i => (distance: double.PositiveInfinity, prev: (uint)i))
				.ToArray();
			info[start].distance = 0;
			var visited = new System.Collections.BitArray(adjacency.Count);

			var heap = new Heap<(uint node, double distance)>((a, b) => a.distance.CompareTo(b.distance));
			heap.Push((start, 0));
			while (heap.Count > 0) {
				(uint node, double distance) current = heap.Pop();
				if (visited[(int)current.node]) continue;
				var edges = adjacency[(int)current.node];
				for (uint n = 0; n < edges.Count; n++) {
					uint v = edges[(int)n].node;
					if (visited[(int)v]) continue;
					double alt = info[current.node].distance + edges[(int)n].weight;
					if (alt < info[v].distance) {
						info[v] = (alt, current.node);
						heap.Push((v, alt));
					}
				}

				visited[(int)current.node] = true;
			}

			return info;
		}
	}

	internal sealed class Heap<T> {
		private readonly IComparer<T> comparer;
		private readonly List<T> list = new List<T> { default };

		public Heap() : this(default(IComparer<T>)) { }

		public Heap(IComparer<T> comparer) {
			this.comparer = comparer ?? Comparer<T>.Default;
		}

		public Heap(Comparison<T> comparison) : this(Comparer<T>.Create(comparison)) { }

		public uint Count => (uint)(list.Count - 1);

		public void Push(T element) {
			list.Add(element);
			SiftUp((uint)(list.Count - 1));
		}

		public T Pop() {
			T result = list[1];
			list[1] = list[list.Count - 1];
			list.RemoveAt(list.Count - 1);
			SiftDown(1);
			return result;
		}

		private static uint Parent(uint i) => i / 2;
		private static uint Left(uint i) => i * 2;
		private static uint Right(uint i) => i * 2 + 1;

		private void SiftUp(uint i) {
			while (i > 1) {
				uint parent = Parent(i);
				if (comparer.Compare(list[(int)i], list[(int)parent]) > 0) return;
				(list[(int)parent], list[(int)i]) = (list[(int)i], list[(int)parent]);
				i = parent;
			}
		}

		private void SiftDown(uint i) {
			for (uint left = Left(i); left < list.Count; left = Left(i)) {
				uint smallest = comparer.Compare(list[(int)left], list[(int)i]) <= 0 ? left : i;
				uint right = Right(i);
				if (right < list.Count && comparer.Compare(list[(int)right], list[(int)smallest]) <= 0) smallest = right;
				if (smallest == i) return;
				(list[(int)i], list[(int)smallest]) = (list[(int)smallest], list[(int)i]);
				i = smallest;
			}
		}
	}
}