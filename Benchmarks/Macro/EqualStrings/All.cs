﻿using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.EqualStrings; 

public class All {
	public static ulong Iterations;
	public static ulong LoopIterations;

	private static void Analyze(string s) {
		StringBuilder sb = new StringBuilder("Examining [");
		sb.Append(s);
		sb.Append("] which has a length of ");
		sb.Append(s.Length);
		sb.Append(":");
		Console.WriteLine(sb.ToString());
		if (s.Length > 1) {
			var b = s[0];
			int index = 1;
			foreach (char c in s[1..]) {
				if (c != b) {
					Console.WriteLine("    Not all characters in the string are the same.");
					StringBuilder sb2 = new StringBuilder("    '");
					sb2.Append(c);
					sb2.Append("' (0x");
					sb2.Append($"{(int)c:X02}");
					sb2.Append(") is different at position ");
					sb2.Append(index);
					Console.WriteLine(sb2.ToString());
					return;
				}

				index++;
			}
		}

		Console.WriteLine("    All characters in the string are the same.");
	}

	[Benchmark("EqualStrings", "")]
	public static int AllEqualStrings() {
		var strs = new string[] { "", "   ", "2", "333", ".55", "tttTTT", "4444 444k" };
		for (ulong i = 0; i < LoopIterations; i++) {
			foreach (string str in strs) {
				Analyze(str);
			}
		}

		return 2048;
	}
}