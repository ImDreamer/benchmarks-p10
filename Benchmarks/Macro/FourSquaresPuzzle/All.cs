﻿using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.FourSquaresPuzzle;

public class All {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("FourSquares", "")]
	public static int AllFourSquares() {
		for (ulong i = 0; i < LoopIterations; i++) {
			FourSquare(1, 7, true, true);
			FourSquare(3, 9, true, true);
			FourSquare(0, 9, false, false);
		}

		//Todo: Fix return value
		return 2028;
	}

	public static void FourSquare(uint low, uint high, bool unique, bool print) {
		uint count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		for (uint a = low; a <= high; ++a) {
			for (uint b = low; b <= high; ++b) {
				if (NotValid(unique, b, a)) continue;

				uint fp = a + b;
				for (uint c = low; c <= high; ++c) {
					if (NotValid(unique, c, b, a)) continue;
					for (uint d = low; d <= high; ++d) {
						if (NotValid(unique, d, c, b, a)) continue;
						if (fp != b + c + d) continue;

						for (uint e = low; e <= high; ++e) {
							if (NotValid(unique, e, d, c, b, a)) continue;
							for (uint f = low; f <= high; ++f) {
								if (NotValid(unique, f, e, d, c, b, a)) continue;
								if (fp != d + e + f) continue;

								for (uint g = low; g <= high; ++g) {
									if (NotValid(unique, g, f, e, d, c, b, a)) continue;
									if (fp != f + g) continue;

									++count;
									if (print) {
										StringBuilder sb = new StringBuilder();
										sb.Append(a);
										sb.Append(" ");
										sb.Append(b);
										sb.Append(" ");
										sb.Append(c);
										sb.Append(" ");
										sb.Append(d);
										sb.Append(" ");
										sb.Append(e);
										sb.Append(" ");
										sb.Append(f);
										sb.Append(" ");
										sb.Append(g);
										Console.WriteLine(sb.ToString());
									}
								}
							}
						}
					}
				}
			}
		}

		if (unique) {
			StringBuilder sb = new StringBuilder("There are ");
			sb.Append(count);
			sb.Append(" unique solutions in [");
			sb.Append(low);
			sb.Append(", ");
			sb.Append(high);
			sb.Append("]");
			Console.WriteLine(sb.ToString());
		}
		else {
			StringBuilder sb = new StringBuilder("There are ");
			sb.Append(count);
			sb.Append(" non-unique solutions in [");
			sb.Append(low);
			sb.Append(", ");
			sb.Append(high);
			sb.Append("]");
			Console.WriteLine(sb.ToString());
		}
	}

	private static bool NotValid(bool unique, uint needle, params uint[] haystack) {
		if (unique) {
			foreach (uint i in haystack) {
				if (i == needle) {
					return true;
				}
			}
		}

		return false;
	}
}