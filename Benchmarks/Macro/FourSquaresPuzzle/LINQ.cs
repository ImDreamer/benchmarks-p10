﻿using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.FourSquaresPuzzle; 

public class LINQ {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("FourSquares", "")]
	public static int LINQFourSquares() {
		for (ulong i = 0; i < LoopIterations; i++) {
			FourSquare(1, 7, true, true);
			FourSquare(3, 9, true, true);
			FourSquare(0, 9, false, false);
		}
		//Todo: Fix return value
		return 2028;
	}

	public static void FourSquare(int low, int high, bool unique, bool print) {
		int count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		for (int a = low; a <= high; ++a) {
			for (int b = low; b <= high; ++b) {
				if (NotValid(unique, b, a)) continue;

				int fp = a + b;
				for (int c = low; c <= high; ++c) {
					if (NotValid(unique, c, b, a)) continue;
					for (int d = low; d <= high; ++d) {
						if (NotValid(unique, d, c, b, a)) continue;
						if (fp != b + c + d) continue;

						for (int e = low; e <= high; ++e) {
							if (NotValid(unique, e, d, c, b, a)) continue;
							for (int f = low; f <= high; ++f) {
								if (NotValid(unique, f, e, d, c, b, a)) continue;
								if (fp != d + e + f) continue;

								for (int g = low; g <= high; ++g) {
									if (NotValid(unique, g, f, e, d, c, b, a)) continue;
									if (fp != f + g) continue;

									++count;
									if (print) {
										Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}", a, b, c, d, e, f, g);
									}
								}
							}
						}
					}
				}
			}
		}

		if (unique) {
			Console.WriteLine("There are {0} unique solutions in [{1}, {2}]", count, low, high);
		}
		else {
			Console.WriteLine("There are {0} non-unique solutions in [{1}, {2}]", count, low, high);
		}
	}

	private static bool NotValid(bool unique, int needle, params int[] haystack) {
		if (unique) {
			foreach (int i in haystack) {
				if (i == needle) {
					return true;
				}
			}
		}

		return false;
	}
}