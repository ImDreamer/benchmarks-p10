﻿using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.HappyNumbers;

public class Collections {
	public static ulong Iterations;
	public static ulong LoopIterations;

	public static bool Ishappy(int n) {
		int[] cache = new int[17];
		int currentIndex = 0;
		int sum = 0;
		while (n != 1) {
			if (cache.Contains(n)) {
				return false;
			}

			cache[currentIndex] = n;
			currentIndex++;
			while (n != 0) {
				int digit = n % 10;
				sum += digit * digit;
				n /= 10;
			}

			n = sum;
			sum = 0;
		}

		return true;
	}

	[Benchmark("HappyNumbers", "")]
	public static int CollectionHappyNumber() {
		for (ulong i = 0; i < LoopIterations; i++) {
			int num = 1;
			int[] happynums = new int[8];
			int currentIndex = 0;

			while (happynums[7] == 0) {
				if (Ishappy(num)) {
					happynums[currentIndex] = num;
					currentIndex++;
				}

				num++;
			}

			Console.WriteLine("First 8 happy numbers : " + string.Join(",", happynums));
		}

		//Todo: Fix return value
		return 2048;
	}
}