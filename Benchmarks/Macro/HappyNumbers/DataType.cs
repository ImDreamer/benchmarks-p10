﻿using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.HappyNumbers;

public class DataType {
	public static ulong Iterations;
	public static ulong LoopIterations;

	public static bool Ishappy(uint n) {
		List<uint> cache = new List<uint>();
		uint sum = 0;
		while (n != 1) {
			if (cache.Contains(n)) {
				return false;
			}

			cache.Add(n);
			while (n != 0) {
				uint digit = n % 10;
				sum += digit * digit;
				n /= 10;
			}

			n = sum;
			sum = 0;
		}

		return true;
	}

	[Benchmark("HappyNumbers", "")]
	public static int DataTypeHappyNumber() {
		for (ulong i = 0; i < LoopIterations; i++) {
			uint num = 1;
			List<uint> happynums = new List<uint>();

			while (happynums.Count < 8) {
				if (Ishappy(num)) {
					happynums.Add(num);
				}

				num++;
			}

			StringBuilder sb = new StringBuilder("First 8 happy numbers : ");
			sb.Append(string.Join(",", happynums));
			Console.WriteLine(sb.ToString());
		}

		//Todo: Fix return value
		return 2048;
	}
}