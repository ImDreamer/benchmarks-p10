﻿using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.HappyNumbers;

public class Default {
	public static ulong Iterations;
	public static ulong LoopIterations;

	public static bool Ishappy(int n) {
		List<int> cache = new List<int>();
		int sum = 0;
		while (n != 1) {
			if (cache.Contains(n)) {
				return false;
			}

			cache.Add(n);
			while (n != 0) {
				int digit = n % 10;
				sum += digit * digit;
				n /= 10;
			}

			n = sum;
			sum = 0;
		}

		return true;
	}

	[Benchmark("HappyNumbers", "")]
	public static int DefaultHappyNumber() {
		for (ulong i = 0; i < LoopIterations; i++) {
			int num = 1;
			List<int> happynums = new List<int>();

			while (happynums.Count < 8) {
				if (Ishappy(num)) {
					happynums.Add(num);
				}

				num++;
			}

			Console.WriteLine("First 8 happy numbers : " + string.Join(",", happynums));
		}

		//Todo: Fix return value
		return 2048;
	}
}