﻿using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.HappyNumbers; 

public class Optimal {
	public static ulong Iterations;
	public static ulong LoopIterations;

	public static bool Ishappy(uint n) {
		uint[] cache = new uint[17];
		uint currentIndex = 0;
		uint sum = 0;
		while (n != 1) {
			if (cache.Contains(n)) {
				return false;
			}

			cache[currentIndex] = n;
			currentIndex++;
			while (n != 0) {
				uint digit = n % 10;
				sum += digit * digit;
				n /= 10;
			}

			n = sum;
			sum = 0;
		}

		return true;
	}

	[Benchmark("HappyNumbers", "")]
	public static int OptimalHappyNumber() {
		for (ulong i = 0; i < LoopIterations; i++) {
			uint num = 1;
			uint[] happynums = new uint[8];
			uint currentIndex = 0;

			while (happynums[7] == 0) {
				if (Ishappy(num)) {
					happynums[currentIndex] = num;
					currentIndex++;
				}

				num++;
			}

			Console.WriteLine($"First 8 happy numbers : {string.Join(",", happynums)}");
		}

		//Todo: Fix return value
		return 2048;
	}
}