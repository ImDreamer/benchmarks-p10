﻿using System.Reflection;
using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.Introspection;

public class All {
	public static ulong Iterations;
	public static ulong LoopIterations;

	public static int bloop = -10;
	public static int bloop2 = -20;

	public static void Main() {
		FieldInfo bloopField = null;


		foreach (FieldInfo field in typeof(All).GetFields()) {
			if (field.Name != "bloop") {
				continue;
			}

			bloopField = field;
			if (bloopField.FieldType != typeof(int)) {
				throw new InvalidProgramException("bloop should be an integer");
			}

			break;
		}


		if (bloopField == null) {
			throw new InvalidProgramException("No bloop exported value");
		}

		foreach (Assembly refAsm in AppDomain.CurrentDomain.GetAssemblies().Where(assembly => !assembly.IsDynamic)) {
			foreach (Type type in refAsm.GetExportedTypes()) {
				if (type.Name == "Math") {
					MethodInfo? absMethod = type.GetMethod("Abs", new Type[] { typeof(int) });
					if (absMethod != null) {
						var absDelegate = (Func<int, int>)Delegate.CreateDelegate(typeof(Func<int, int>), absMethod);
						StringBuilder sb = new StringBuilder("bloop's abs value = ");
						sb.Append(absDelegate((int)bloopField.GetValue(null)));
						Console.WriteLine(sb.ToString());
					}
				}
			}
		}

		uint intCount = 0;
		int total = 0;


		foreach (FieldInfo field in typeof(All).GetFields()) {
			if (field.FieldType == typeof(int)) {
				intCount++;
				total += (int)field.GetValue(null);
			}
		}


		StringBuilder sb2 = new StringBuilder();
		sb2.Append(intCount);
		sb2.Append(" exported ints which total to ");
		sb2.Append(total);
		Console.WriteLine(sb2.ToString());
	}

	[Benchmark("Introspection", "")]
	public static int AllIntrospection() {
		for (ulong i = 0; i < LoopIterations; i++) {
			Main();
		}

		return 2048;
	}
}