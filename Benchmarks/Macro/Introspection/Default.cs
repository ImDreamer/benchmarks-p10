﻿using System;
using System.Reflection;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.Introspection;

public class Default {
	public static ulong Iterations;
	public static ulong LoopIterations;

	public static int bloop = -10;
	public static int bloop2 = -20;

	public static void Main() {
		FieldInfo bloopField = null;


		foreach (FieldInfo field in typeof(Default).GetFields()) {
			if (field.Name != "bloop") {
				continue;
			}

			bloopField = field;
			if (bloopField.FieldType != typeof(int)) {
				throw new InvalidProgramException("bloop should be an integer");
			}

			break;
		}


		if (bloopField == null) {
			throw new InvalidProgramException("No bloop exported value");
		}

		foreach (Assembly refAsm in AppDomain.CurrentDomain.GetAssemblies().Where(assembly => !assembly.IsDynamic)) {
			foreach (Type type in refAsm.GetExportedTypes()) {
				if (type.Name == "Math") {
					MethodInfo? absMethod = type.GetMethod("Abs", new Type[] { typeof(int) });
					if (absMethod != null) {
						Console.WriteLine("bloop's abs value = {0}",
							absMethod.Invoke(null, new object[] { bloopField.GetValue(null) }));
					}
				}
			}
		}

		int intCount = 0;
		int total = 0;

		foreach (FieldInfo field in typeof(Default).GetFields()) {
			if (field.FieldType == typeof(int)) {
				intCount++;
				total += (int)field.GetValue(null);
			}
		}


		Console.WriteLine("{0} exported ints which total to {1}", intCount, total);
	}

	[Benchmark("Introspection", "")]
	public static int DefaultIntrospection() {
		for (ulong i = 0; i < LoopIterations; i++) {
			Main();
		}

		return 2048;
	}
}