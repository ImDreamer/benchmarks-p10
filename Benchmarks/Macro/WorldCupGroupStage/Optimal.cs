﻿using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Macro.WorldCupGroupStage; 

public class Optimal {
	public static ulong Iterations;
	public static ulong LoopIterations;
	private static uint[][] _histogram;

	[Benchmark("WorldCupGroupStage", "")]
	public static string OptimalWorldCupStage() {
		string result = "";
		for (ulong loopIterations = 0; loopIterations < LoopIterations; loopIterations++) {
			Create();
			result = ToString();
		}

		return result;
	}

	private static void Create() {
		uint[] scoring = new[] { 0u, 1u, 3u };

		_histogram = new uint[4][];
		for (uint i = 0; i < 4; i++) {
			_histogram[i] = new uint[10];
		}

		List<List<int>> teamCombos = CartesianProduct2(new List<int> { 0, 1, 2, 3 }, new List<int> { 0, 1, 2, 3 });
		List<List<int>> tmp2 = CartesianProduct6(new List<int> { 0, 1, 2 }, new List<int> { 0, 1, 2 });


		foreach (var results in tmp2) {
			var points = new uint[4];

			var zipped = new List<(uint result, List<int> teams)>();
			for (var index = 0; index < teamCombos.Count; index++) {
				List<int> ints = teamCombos[index];
				zipped.Add(((uint)results.ElementAt(index), ints));
			}

			foreach ((uint result, List<int> teams) in zipped) {
				points[teams[0]] += scoring[result];
				points[teams[1]] += scoring[2 - result];
			}

			Array.Sort(points, (i, i1) => (int)(i1 - i));

			for (uint i = 0; i < points.Length; i++) {
				uint p = points[i];
				_histogram[i][p]++;
			}
		}
	}


	private static List<List<T>> CartesianProduct2<T>(List<T> input1, List<T> input2) {
		List<List<T>> combos = new List<List<T>>();

		foreach (T val1 in input1) {
			foreach (T val2 in input2) {
				if (val1.Equals(val2)) {
					continue;
				}

				bool found = false;
				foreach (List<T> list in combos) {
					if (list[0].Equals(val1) && list[1].Equals(val2) ||
					    list[1].Equals(val1) && list[0].Equals(val2)) {
						found = true;
					}
				}

				if (found) {
					continue;
				}

				var combo = new List<T> {
					val1,
					val2
				};

				combos.Add(combo);
			}
		}

		return combos;
	}

	private static List<List<T>> CartesianProduct6<T>(List<T> input1, List<T> input2) {
		List<List<T>> combos = new List<List<T>>();

		foreach (T val1 in input1) {
			foreach (T val2 in input2) {
				foreach (T val3 in input1) {
					foreach (T val4 in input2) {
						foreach (T val5 in input1) {
							foreach (T val6 in input2) {
								var combo = new List<T> {
									val1,
									val2,
									val3,
									val4,
									val5,
									val6
								};

								combos.Add(combo);
							}
						}
					}
				}
			}
		}


		return combos;
	}

	private static new string ToString() {
		var sb = new StringBuilder();

		const string range = "0   1   2   3   4   5   6   7   8   9   ";
		sb.Append("Points      : ");
		sb.AppendLine(range);
		const string u = "─────────────────────────────────────────────────────";
		sb.AppendLine(u);

		var places = new[] { "First", "Second", "Third", "Fourth" };
		for (uint i = 0; i < _histogram.Length; i++) {
			uint[] r = _histogram[i];
			(uint i, uint[] r) row = (i, r);
			sb.Append($"{places[row.i],-6} place: ");
			foreach (int standing in row.r) {
				sb.Append($"{standing,-3} ");
			}

			sb.Append("\n");
		}

		return sb.ToString();
	}
}