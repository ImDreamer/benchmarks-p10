﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Micro;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
[SuppressMessage("ReSharper", "UnusedType.Global")]
public class ExceptionBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("Exception", "Tests try-catch where a cached ArgumentException is thrown")]
	public static ulong CachedArgumentException() {
		ArgumentException argumentException = new ArgumentException();

		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw argumentException;
			}
			catch { }
		}

		return LoopIterations;
	}
	
	[Benchmark("Exception", "Tests try-catch where a cached DivideByZeroException is thrown")]
	public static ulong CachedDivideByZeroException() {
		DivideByZeroException divideByZeroException = new DivideByZeroException();

		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw divideByZeroException;
			}
			catch { }
		}

		return LoopIterations;
	}
	
	[Benchmark("Exception", "Tests try-catch where a cached Exception is thrown")]
	public static ulong CachedException() {
		Exception exception = new Exception();

		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw exception;
			}
			catch { }
		}

		return LoopIterations;
	}

	[Benchmark("Exception", "Tests try-catch with a new ArgumentException thrown")]
	public static ulong NewArgumentException() {
		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw new ArgumentException();
			}
			catch { }
		}

		return LoopIterations;
	}
	
	[Benchmark("Exception", "Tests try-catch with a new DivideByZeroException thrown")]
	public static ulong NewDivideByZeroException() {
		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw new DivideByZeroException();
			}
			catch { }
		}

		return LoopIterations;
	}
	
	[Benchmark("Exception", "Tests try-catch with a new Exception thrown")]
	public static ulong NewException() {
		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw new Exception();
			}
			catch { }
		}

		return LoopIterations;
	}

	[Benchmark("Exception", "Tests try-catch a cached ArgumentException is thrown and addition occurs")]
	public static ulong ThrowCachedArgumentException() {
		ArgumentException argumentException = new ArgumentException();
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw argumentException;
			}
			catch {
				result += i;
			}
		}

		return result;
	}
	
	[Benchmark("Exception", "Tests try-catch a cached DivideByZeroException is thrown and addition occurs")]
	public static ulong ThrowCachedDivideByZeroException() {
		DivideByZeroException divideByZeroException = new DivideByZeroException();
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw divideByZeroException;
			}
			catch {
				result += i;
			}
		}

		return result;
	}
	
	[Benchmark("Exception", "Tests try-catch a cached Exception is thrown and addition occurs")]
	public static ulong ThrowCachedException() {
		Exception exception = new Exception();
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw exception;
			}
			catch {
				result += i;
			}
		}

		return result;
	}

	[Benchmark("Exception", "Tests try-catch with a new ArgumentException thrown and addition occurs")]
	public static ulong ThrowNewArgumentException() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw new ArgumentException();
			}
			catch {
				result += i;
			}
		}

		return result;
	}
	
	[Benchmark("Exception", "Tests try-catch with a new DivideByZeroException thrown and addition occurs")]
	public static ulong ThrowNewDivideByZeroException() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw new DivideByZeroException();
			}
			catch {
				result += i;
			}
		}

		return result;
	}
	
	[Benchmark("Exception", "Tests try-catch with a new Exception thrown and addition occurs")]
	public static ulong ThrowNewException() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			try {
				throw new Exception();
			}
			catch {
				result += i;
			}
		}

		return result;
	}

	[Benchmark("Exception Creation", "Tests how much it costs to create an ArgumentException")]
	public static Exception CreateArgumentException() {
		Exception result = new Exception();

		for (ulong i = 0; i < LoopIterations; i++) {
			result = new ArgumentException();
		}

		return result;
	}
	
	[Benchmark("Exception Creation", "Tests how much it costs to create a DivideByZeroException")]
	public static Exception CreateDivideByZeroException() {
		Exception result = new Exception();

		for (ulong i = 0; i < LoopIterations; i++) {
			result = new DivideByZeroException();
		}

		return result;
	}
	
	[Benchmark("Exception Creation", "Tests how much it costs to create an Exception")]
	public static Exception CreateException() {
		Exception result = new Exception();

		for (ulong i = 0; i < LoopIterations; i++) {
			result = new Exception();
		}

		return result;
	}
}