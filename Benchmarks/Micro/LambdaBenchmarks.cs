﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks.Micro;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class LambdaBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("Lambda Expression", "Tests if using a func is better")]
	public static ulong Lambda() {
		ulong result = 0;
		Func<ulong> test = () => 25;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = test() + result + i;
		}

		return result;
	}

	[Benchmark("Lambda Expression", "Tests if using a func with closure is better")]
	public static ulong LambdaClosure() {
		ulong result = 0;
		Func<ulong> test = () => 25 + result;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = test() + i;
		}

		return result;
	}

	[Benchmark("Lambda Expression", "Tests if using an action is better")]
	public static ulong LambdaAction() {
		ulong result = 0;
		Action<ulong> test = value => result = 25 + value;
		for (ulong i = 0; i < LoopIterations; i++) {
			test(result + i);
		}

		return result;
	}

	[Benchmark("Lambda Expression", "Tests if using a func with param is better")]
	public static ulong LambdaParameter() {
		ulong result = 0;
		Func<ulong, ulong> test = value => 25 + value;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = test(result) + i;
		}

		return result;
	}

	private delegate ulong PerformCalculation();

	[Benchmark("Lambda Expression", "Tests if using a delegate is better")]
	public static ulong LambdaDelegate() {
		ulong result = 0;
		PerformCalculation test = () => 25;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = test() + result + i;
		}

		return result;
	}

	[Benchmark("Lambda Expression Inside Loop", "Tests if using a func is better inside the loop")]
	public static ulong InsideLoopLambda() {
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			ulong i1 = i;
			Func<ulong> test = () => 25 + i1;
			result = test() + result;
		}

		return result;
	}

	[Benchmark("Lambda Expression Inside Loop", "Tests if using a func with closure is better inside the loop")]
	public static ulong InsideLoopLambdaClosure() {
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			ulong i1 = i;
			Func<ulong> test = () => 25 + result + i1;
			result = test();
		}

		return result;
	}

	[Benchmark("Lambda Expression Inside Loop", "Tests if using an action is better inside the loop")]
	public static ulong InsideLoopLambdaAction() {
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			ulong i1 = i;
			Action<ulong> test = value => result = 25 + value + i1;
			test(result);
		}

		return result;
	}

	[Benchmark("Lambda Expression Inside Loop", "Tests if using a func with param is better inside the loop")]
	public static ulong InsideLoopLambdaParameter() {
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			ulong i1 = i;
			Func<ulong, ulong> test = value => 25 + value + i1;
			result = test(result);
		}

		return result;
	}

	[Benchmark("Lambda Expression Inside Loop", "Tests if using a delegate is better inside the loop")]
	public static ulong InsideLoopLambdaDelegate() {
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			ulong i1 = i;
			PerformCalculation test = () => 25 + i1;
			result = test() + result;
		}

		return result;
	}
}